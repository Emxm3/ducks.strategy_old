﻿using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.ViewModel.MainWindow;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory._Mock
{
    public class MockMainWindowViewModel : IMainWindowViewModel
    {
        public ObservableCollection<Duck> ListOfDucks { get; set; }

        public MockMainWindowViewModel()
        {
            ListOfDucks = new()
            {
                new Mallard(),
                new LongTailed(),
                new AmericanBlack(),
                new Muscovy()
            };
        }
    }
}
