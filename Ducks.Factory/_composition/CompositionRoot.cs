﻿using Ducks.Factory.MVVM.Model.Ducks;
using Ducks.Factory.MVVM.ViewModel.MainWindow;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Ducks.Factory._composition
{
    public class CompositionRoot : BaseRoot
    {
        [STAThread]
        public static void Main()
        {
            new CompositionRoot().Execute<MainWindow, IMainWindowViewModel>();
        }

        public override void CustomInjections(Container container)
        {
            var existingAssemblies = new List<Assembly>() { GetType().Assembly };
            var asSingleton = Lifestyle.Singleton;

            //Register ducks in all one assemblies, and assign them as singleton
            container.Collection.Register<Duck>(existingAssemblies, asSingleton);
        }
    }
}
