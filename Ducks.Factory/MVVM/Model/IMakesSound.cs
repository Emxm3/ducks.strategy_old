﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model
{
    public interface IMakesSound
    {
        void MakeSound();
    }
}
