﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks
{
    public class Muscovy : Duck
    {
        public Muscovy()
        {
            Name = "Muscovy";

            Fly();
            MakeSound();
        }

        public override void Fly()
        {
            FlightDescription = "Flaps its wings";
        }

        public override void MakeSound()
        {
            Sound = "Muscovy Quack";
        }
    }
}
