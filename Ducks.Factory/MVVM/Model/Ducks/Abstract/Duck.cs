﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks
{
    public abstract class Duck : ICanFly, IMakesSound
    {
        public string Name { get; set; }
        public string FlightDescription { get; set; }
        public string Sound { get; set; }

        public abstract void Fly();

        public abstract void MakeSound();
    }
}
