﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks
{


    public class CustomDuck : Duck, INotifyPropertyChanged
    {
        private FlightEnum _FlightMethod;
        private SoundEnum _SoundMethod;

        public FlightEnum FlightMethod { get => _FlightMethod; set { _FlightMethod = value; NotifyPropertyChanged(); UpdateDuck(); } }
        public SoundEnum SoundMethod { get => _SoundMethod; set { _SoundMethod = value; NotifyPropertyChanged(); UpdateDuck(); } }

 

        public CustomDuck()
        {
            Name = "Custom Duck";
            FlightMethod = FlightEnum.Wings;
            SoundMethod = SoundEnum.NoSound;

            
        }

        private void UpdateDuck()
        {
            Fly();
            MakeSound();

           
        }

        public override void Fly()
        {
            switch(FlightMethod)
            {
                case FlightEnum.Wings:
                    FlightDescription = "Flaps its wings";
                    break;
                case FlightEnum.Engine:
                    FlightDescription = "Uses jet engines!";
                    break;
                case FlightEnum.NoFlight:
                    FlightDescription = "Doesn't fly";
                    break;
            }
            NotifyPropertyChanged(nameof(FlightDescription));
          
        }

        public override void MakeSound()
        {
            switch (SoundMethod)
            {
                case SoundEnum.NoSound:
                    Sound = "No Sound is made";
                    break;
                case SoundEnum.Quack:
                    Sound = "Quacks like a duck";
                    break;
                case SoundEnum.SonicBoom:
                    Sound = "SONIC BOOM!!";
                    break;
            }
            NotifyPropertyChanged(nameof(Sound));
        }



        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string name = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

    }
}
