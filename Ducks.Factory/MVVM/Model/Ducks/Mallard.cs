﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks
{
    public class Mallard : Duck
    {
        public Mallard()
        {
            Name = "Mallard Duck";

            Fly();
            MakeSound();
        }

        public override void Fly()
        {
            FlightDescription = "Uses its wings";
        }

        public override void MakeSound()
        {
            Sound = "Mallard Quack";
        }
    }
}
