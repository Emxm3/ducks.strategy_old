﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks
{
    public class AmericanBlack : Duck 
    {
        public AmericanBlack()
        {
            Name = "American Black";

            Fly();
            MakeSound();
        }

        public override void Fly()
        {
            FlightDescription = "Flaps its wings";
        }

        public override void MakeSound()
        {
            Sound = "American Black Quack";
        }
    }
}
