﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks
{
    public class LongTailed : Duck
    {
        public LongTailed()
        {
            Name = "Long-Tailed";

            Fly();
            MakeSound();
        }

        public override void Fly()
        {
            FlightDescription = "Flaps both wings";
        }

        public override void MakeSound()
        {
            Sound = "Long-Tailed Quack";
        }
    }
}
