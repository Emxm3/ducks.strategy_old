﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks.OtherDucks
{
    public class JetpackDuck : Duck
    {

        public JetpackDuck()
        {
            Name = "Jetpack Duck";
            Fly();
            MakeSound();
        }
        public override void Fly()
        {
            FlightDescription = "Uses jet engines";
        }

        public override void MakeSound()
        {
            Sound = "Sonic Boom";
        }
    }
}
