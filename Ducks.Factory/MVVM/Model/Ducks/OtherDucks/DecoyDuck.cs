﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks.OtherDucks
{
    public class DecoyDuck : Duck
    {
        public DecoyDuck()
        {
            Name = "Decoy Duck";
            Fly();
            MakeSound();
        }
        public override void Fly()
        {
            FlightDescription = "Doesn't fly";
        }

        public override void MakeSound()
        {
            Sound = "Fake Duck sound";
        }
    }
}
