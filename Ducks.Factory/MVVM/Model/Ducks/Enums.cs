﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ducks.Factory.MVVM.Model.Ducks
{

    public enum FlightEnum
    {
        NoFlight,
        Wings,
        Engine
    }

    public enum SoundEnum
    {
        NoSound,
        Quack,
        SonicBoom,
    }
}
