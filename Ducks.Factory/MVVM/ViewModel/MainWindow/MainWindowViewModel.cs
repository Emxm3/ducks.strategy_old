﻿using Ducks.Factory._composition;
using Ducks.Factory.MVVM.Model.Ducks;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace Ducks.Factory.MVVM.ViewModel.MainWindow
{
    [AutoWire(AsSingleton: true)]
    public class MainWindowViewModel : IMainWindowViewModel
    {
        public ObservableCollection<Duck> ListOfDucks { get; set; }

        public List<FlightEnum> Flights { get => Enum.GetValues(typeof(FlightEnum)).Cast<FlightEnum>().ToList(); }
        public List<SoundEnum> Sounds { get => Enum.GetValues(typeof(SoundEnum)).Cast<SoundEnum>().ToList(); }



        public MainWindowViewModel(IEnumerable<Duck> Ducks)
        {
            ListOfDucks = new ObservableCollection<Duck>(Ducks);
        }

    }

    public interface IMainWindowViewModel
    {
        ObservableCollection<Duck> ListOfDucks { get; set; }
    }
}
